if( StatdollWndLight == nil ) then
  StatdollWndLight = {}
end

------------------------------
-- StatdollWndLight
--
StatdollWndLight = {
  window = "StatdollWndLight",
  defaultWidth  = 260,
  defaultHeight = 390,
  powerHeight   = 0,
  addHeight     = 0,

  lastAnchor = "Disrupt",
}

function StatdollWndLight.onInitialize()
  -- creating the window
  CreateWindow(StatdollWndLight.window, false)

  -- reposition window
  WindowAddAnchor(StatdollWndLight.window, "topleft", "Root", "topleft",
    Statdoll.settings["ui-light-pos-x"],
    Statdoll.settings["ui-light-pos-y"])
    
  StatdollWndLight.lastAnchor = "Disrupt"

  --
  if(Statdoll.settings["ui-light-show-healpower"] or Statdoll.settings["ui-light-show-magicpower"]
    or Statdoll.settings["ui-light-show-rangepower"] or Statdoll.settings["ui-light-show-meleepower"]) then

    -- create separator
    CreateWindowFromTemplate("SeparatorDefStats2", "StatdollWndLightSeparatorTemplate", StatdollWndLight.window)
    WindowAddAnchor("SeparatorDefStats2", "bottom", StatdollWndLight.lastAnchor, "top", 0, 0)
    StatdollWndLight.lastAnchor = "SeparatorDefStats2"
    StatdollWndLight.powerHeight = 12

    -- create healpow + healcrit
    if(Statdoll.settings["ui-light-show-healpower"]) then
      StatdollWndLight.createStatRow(Statdoll.Stats.values[Statdoll._HEALPOWER], StatdollWndLight.lastAnchor, StatdollWndLight.window)
      StatdollWndLight.lastAnchor = StatdollWndLight.createStatRow(Statdoll.Stats.values[Statdoll._HEALCRIT], Statdoll.Stats.values[Statdoll._HEALPOWER]["refWnd"], StatdollWndLight.window)
      StatdollWndLight.powerHeight = StatdollWndLight.powerHeight + 42
    end
    
    -- create magicpow + magiccrit
    if(Statdoll.settings["ui-light-show-magicpower"]) then
      StatdollWndLight.createStatRow(Statdoll.Stats.values[Statdoll._MAGICPOWER], StatdollWndLight.lastAnchor, StatdollWndLight.window)
      StatdollWndLight.lastAnchor = StatdollWndLight.createStatRow(Statdoll.Stats.values[Statdoll._MAGICCRIT], Statdoll.Stats.values[Statdoll._MAGICPOWER]["refWnd"], StatdollWndLight.window)
      StatdollWndLight.powerHeight = StatdollWndLight.powerHeight + 42
    end
    
    -- create rangepow + rangecrit
    if(Statdoll.settings["ui-light-show-rangepower"]) then
      StatdollWndLight.createStatRow(Statdoll.Stats.values[Statdoll._RANGEPOWER], StatdollWndLight.lastAnchor, StatdollWndLight.window)
      StatdollWndLight.lastAnchor = StatdollWndLight.createStatRow(Statdoll.Stats.values[Statdoll._RANGECRIT], Statdoll.Stats.values[Statdoll._RANGEPOWER]["refWnd"], StatdollWndLight.window)
      StatdollWndLight.powerHeight = StatdollWndLight.powerHeight + 42
    end

    -- create meleepow + meleecrit
    if(Statdoll.settings["ui-light-show-meleepower"]) then
      StatdollWndLight.createStatRow(Statdoll.Stats.values[Statdoll._MELEEPOWER], StatdollWndLight.lastAnchor, StatdollWndLight.window)
      StatdollWndLight.lastAnchor = StatdollWndLight.createStatRow(Statdoll.Stats.values[Statdoll._MELEECRIT], Statdoll.Stats.values[Statdoll._MELEEPOWER]["refWnd"], StatdollWndLight.window)
      StatdollWndLight.powerHeight = StatdollWndLight.powerHeight + 42
    end
          
    WindowSetDimensions(StatdollWndLight.window, StatdollWndLight.defaultWidth, (StatdollWndLight.defaultHeight + StatdollWndLight.powerHeight))
  end

  -- set labels and icons
  StatdollWndLight.setLabels()
end

function StatdollWndLight.onShutdown()
  Statdoll.settings["ui-light-pos-x"], Statdoll.settings["ui-light-pos-y"] = WindowGetOffsetFromParent(StatdollWndLight.window)
  DestroyWindow(StatdollWndLight.window)
end

function StatdollWndLight.onUpdate()
  for i, value in pairs(Statdoll.Stats.values) do
    local refWnd = value["refWnd"]
    if (value["refWndX"] ~= nil and value["refWndX"] ~= "") then
      refWnd = refWnd .. value["refWndX"]
    end

    if (DoesWindowExist(refWnd .. "Value")) then
      LabelSetText(refWnd .. "Value", towstring(value["txtValue"]))
      LabelSetTextColor(refWnd .. "Value", value["color-r"], value["color-g"], value["color-b"])
    end

    if (DoesWindowExist(refWnd .. "PowerValue")) then
      LabelSetText(refWnd .. "PowerValue", towstring(value["txtPowerValue"]))
      LabelSetTextColor(refWnd .. "PowerValue", value["color-r"], value["color-g"], value["color-b"])
    end
  end
  
  -- destroy previous window, if it exists
  StatdollWndLight.addHeight = 0
  if(DoesWindowExist("StatdollWndLightAddValues")) then
    DestroyWindow("StatdollWndLightAddValues")
    WindowSetDimensions(StatdollWndLight.window, StatdollWndLight.defaultWidth, (StatdollWndLight.defaultHeight + StatdollWndLight.powerHeight))
  end

  -- check for additional values from getstat
  if(#Statdoll.Stats.additionalValues > 0 and Statdoll.settings["ui-light-additional"]) then
    -- create window
    CreateWindowFromTemplate("StatdollWndLightAddValues", "StatdollWndLightAddValuesTemplate", StatdollWndLight.window)
    WindowAddAnchor("StatdollWndLightAddValues", "bottom", StatdollWndLight.lastAnchor, "top", 0, 0)

    local anchor = "StatdollWndLightAddValuesSeparator"

    -- add row for each additional value
    for i, statArray in pairs(Statdoll.Stats.additionalValues) do
      anchor = StatdollWndLight.createStatRow(statArray, anchor, "StatdollWndLightAddValues")
    end

    StatdollWndLight.addHeight = 12 + (21 * #Statdoll.Stats.additionalValues)    
    WindowSetDimensions("StatdollWndLightAddValues", 230, StatdollWndLight.addHeight)
  end
  
  WindowSetDimensions(StatdollWndLight.window, StatdollWndLight.defaultWidth, 
  (StatdollWndLight.defaultHeight + StatdollWndLight.powerHeight + StatdollWndLight.addHeight))
end

function StatdollWndLight.setLabels()
  for i, value in pairs(Statdoll.Stats.values) do
    local refWnd = value["refWnd"]
    if (value["refWndX"] ~= nil and value["refWndX"] ~= "") then
      refWnd = refWnd .. value["refWndX"]
    end

    if (DoesWindowExist(refWnd .. "Label")) then
      LabelSetText(refWnd .. "Label", towstring(value["txtLabelS"]))
    end

    if (DoesWindowExist(refWnd .. "Icon")) then
      LabelSetText(refWnd .. "Icon", towstring(value["txtIcon"]))
    end

    if (DoesWindowExist(refWnd .. "PowerIcon")) then
      LabelSetText(refWnd .. "PowerIcon", towstring(value["txtPowerIcon"]))
    end

    if (DoesWindowExist(refWnd .. "Value")) then
      LabelSetText(refWnd .. "Value", L"0")
      LabelSetTextColor(refWnd .. "Value", value["color-r"], value["color-g"], value["color-b"])
    end

    if (DoesWindowExist(refWnd .. "PowerValue")) then
      LabelSetText(refWnd .. "PowerValue", L"0")
      LabelSetTextColor(refWnd .. "PowerValue", value["color-r"], value["color-g"], value["color-b"])
    end
  end
end

function StatdollWndLight.setVisible(visible)
  if(visible == true) then
    WindowSetShowing(StatdollWndLight.window, true)
  else
    WindowSetShowing(StatdollWndLight.window, false)
  end
end

function StatdollWndLight.setScaleFactor(scale)
  scale = scale * WindowGetScale("Root")
  WindowSetScale(StatdollWndLight.window, scale)
end

function StatdollWndLight.setCloseButtonVisible(visible)
  WindowSetShowing(StatdollWndLight.window .. "CloseButton", visible)
end

function StatdollWndLight.hide()
  WindowSetShowing(StatdollWndLight.window, false)
end

function StatdollWndLight.setBackground(visible)
  WindowSetShowing(StatdollWndLight.window .. "Background", visible)
end

function StatdollWndLight.setLocked(movable)
  WindowSetMovable(StatdollWndLight.window, not movable)
end

function StatdollWndLight.setBackgroundAlpha(alpha)
  WindowSetAlpha(StatdollWndLight.window .. "Background", alpha)
end

function StatdollWndLight.createStatRow(statArray, anchor, parent)
  local refWnd = statArray["refWnd"]
  if (statArray["refWndX"] ~= nil and statArray["refWndX"] ~= "") then
    refWnd = refWnd .. statArray["refWndX"]
  end
  
  -- create stats row
  CreateWindowFromTemplate(refWnd, "StatdollWndStatLightRowTemplate", parent)
  WindowAddAnchor(refWnd, "bottom", anchor, "top", 0, 0)

  lastAnchor = statArray["refWnd"]

  -- set label and icons
  LabelSetText(statArray["refWnd"] .. "Icon", towstring(statArray["txtIcon"]))
  LabelSetText(statArray["refWnd"] .. "Label", towstring(statArray["txtLabelS"]))
  LabelSetText(statArray["refWnd"] .. "PowerIcon", L"")

  -- set value and color
  LabelSetText(statArray["refWnd"] .. "Value", towstring(statArray["txtValue"]))
  LabelSetTextColor(statArray["refWnd"] .. "Value", statArray["color-r"], statArray["color-g"], statArray["color-b"])
  LabelSetText(statArray["refWnd"] .. "PowerValue", L"")
  
  return refWnd
end
