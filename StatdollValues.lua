if (Statdoll == nil) then
  Statdoll = {}
end

if (Statdoll.Stats == nil) then
  Statdoll.Stats = {}
end

------------------------------------------------
-- Constants
Statdoll._STRENGTH      = 1
Statdoll._BALLISTIC     = 2
Statdoll._INTELLIGENCE  = 3 
Statdoll._WILLPOWER     = 4
Statdoll._WEAPONSKILL   = 5
Statdoll._INITIATIVE    = 6
Statdoll._TOUGHNESS     = 7
Statdoll._WOUNDS        = 8
Statdoll._ARMOR         = 9
Statdoll._SPIRITUAL     = 10
Statdoll._ELEMENTAL     = 11
Statdoll._CORPOREAL     = 12
Statdoll._BLOCK         = 13
Statdoll._PARRY         = 14
Statdoll._DODGE         = 15
Statdoll._DISRUPT       = 16
Statdoll._HEALPOWER     = 17
Statdoll._HEALCRIT      = 18
Statdoll._MAGICPOWER    = 19
Statdoll._MAGICCRIT     = 20
Statdoll._RANGEPOWER    = 21
Statdoll._RANGECRIT     = 22
Statdoll._RANGEAAPOWER  = 23
Statdoll._RANGEDPS      = 24
Statdoll._MELEEPOWER    = 25
Statdoll._MELEECRIT     = 26
Statdoll._MELEEAAPOWER  = 27
Statdoll._MELEEDPS      = 28

------------------------------------------------
-- Stat Arrays

Statdoll.Stats.values = {}
Statdoll.Stats.additionalValues = {}

-- Strength
Statdoll.Stats.values[Statdoll._STRENGTH] = {
  ["refWnd"]       = "Strength",
  ["lookupStr"]    = "Strength",
  
  ["txtLabel"]     = L"Strength",
  ["txtLabelS"]    = L"Str:",
  ["txtIcon"]      = L"<icon=100>",
  ["txtPowerIcon"] = L"<icon=156>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtPowerValue"] = L"+"..math.floor(statArray["value"][1] / 5)
            Statdoll.Stats.formatBaseStat(statArray);
           end,
}

-- Ballistic Skill
Statdoll.Stats.values[Statdoll._BALLISTIC] = {
  ["refWnd"]       = "Ballistic",
  ["lookupStr"]    = "BallisticSkill",
  
  ["txtLabel"]     = L"Ballistic",
  ["txtLabelS"]    = L"Bal:",
  ["txtIcon"]      = L"<icon=107>",
  ["txtPowerIcon"] = L"<icon=156>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtPowerValue"] = L"+"..math.floor(statArray["value"][1] / 5)
            Statdoll.Stats.formatBaseStat(statArray);
           end,
}

-- Intelligence
Statdoll.Stats.values[Statdoll._INTELLIGENCE] = {
  ["refWnd"]       = "Intelligence",
  ["lookupStr"]    = "Intelligence",
  
  ["txtLabel"]     = L"Intelligence",
  ["txtLabelS"]    = L"Int:",
  ["txtIcon"]      = L"<icon=108>",
  ["txtPowerIcon"] = L"<icon=156>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",

  format = function(statArray)
            statArray["txtPowerValue"] = L"+"..math.floor(statArray["value"][1] / 5)
            Statdoll.Stats.formatBaseStat(statArray);
           end, 
}

-- Willpower
Statdoll.Stats.values[Statdoll._WILLPOWER] = {
  ["refWnd"]       = "Willpower",
  ["lookupStr"]    = "Willpower",
  
  ["txtLabel"]     = L"Willpower",
  ["txtLabelS"]    = L"Will:",
  ["txtIcon"]      = L"<icon=102>",
  ["txtPowerIcon"] = L"<icon=156>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtPowerValue"] = L"+"..math.floor(statArray["value"][1] / 5)
            Statdoll.Stats.formatBaseStat(statArray);
           end,
}

-- Weaponskill
Statdoll.Stats.values[Statdoll._WEAPONSKILL] = {
  ["refWnd"]       = "Weaponskill",
  ["lookupStr"]    = "WeaponSkill",
  
  ["txtLabel"]     = L"Weaponskill",
  ["txtLabelS"]    = L"Wpn:",
  ["txtIcon"]      = L"<icon=106>",
  ["txtPowerIcon"] = L"<icon=166>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtPowerValue"] = math.floor(((statArray["value"][1] / (Statdoll.Stats.playerLevel * 7.5 + 50) *.25) *100.0))..L"%"
            Statdoll.Stats.formatBaseStat(statArray);
           end,
}

-- Initiative
Statdoll.Stats.values[Statdoll._INITIATIVE] = {
  ["refWnd"]       = "Initiative",
  ["lookupStr"]    = "Initiative",
  
  ["txtLabel"]     = L"Initiative",
  ["txtLabelS"]    = L"Ini:",
  ["txtIcon"]      = L"<icon=105>",
  ["txtPowerIcon"] = L"<icon=105>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtPowerValue"] = "0%"
            if(statArray["value"][1] > 0) then
              statArray["txtPowerValue"] = math.floor((Statdoll.Stats.playerLevel*7.5+50)/10/(statArray["value"][1])*100)..L"%"
            end
            Statdoll.Stats.formatBaseStat(statArray);
           end,
}

-- Toughness
Statdoll.Stats.values[Statdoll._TOUGHNESS] = {
  ["refWnd"]       = "Toughness",
  ["lookupStr"]    = "Toughness",
  
  ["txtLabel"]     = L"Toughness",
  ["txtLabelS"]    = L"Tou:",
  ["txtIcon"]      = L"<icon=103>",
  ["txtPowerIcon"] = L"<icon=103>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtPowerValue"] = L"-"..math.floor((statArray["value"][1]/5))
            Statdoll.Stats.formatBaseStat(statArray);
           end,
}

-- Wounds
Statdoll.Stats.values[Statdoll._WOUNDS] = {
  ["refWnd"]       = "Wounds",
  ["lookupStr"]    = "Wounds",
  
  ["txtLabel"]     = L"Wounds",
  ["txtLabelS"]    = L"Wou:",
  ["txtIcon"]      = L"<icon=105>",
  ["txtPowerIcon"] = L"<icon=161>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtPowerValue"] = math.floor(statArray["value"][1] * 10)
           end,
}

-- Armor
Statdoll.Stats.values[Statdoll._ARMOR] = {
  ["refWnd"]       = "Armor",
  ["lookupStr"]    = "Armor",
  
  ["txtLabel"]     = L"Armor",
  ["txtLabelS"]    = L"Arm:",
  ["txtIcon"]      = L"<icon=121>",
  ["txtPowerIcon"] = L"<icon=121>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function (statArray)
              statArray["txtPowerValue"] = wstring.format(L"%.01f", (statArray["value"][1] * 0.909) / Statdoll.Stats.playerLevel)..L"%"
              if ((statArray["value"][1] * 0.909 / Statdoll.Stats.playerLevel) > 75) then
                  statArray["color-r"] = 255
                  statArray["color-g"] = 128
                  statArray["color-b"] = 0
              end 
           end,
}

-- Spirit Resist
Statdoll.Stats.values[Statdoll._SPIRITUAL] = {
  ["refWnd"]       = "SpiritResist",
  ["lookupStr"]    = "SpiritResistance",
  
  ["txtLabel"]     = L"Spiritual",
  ["txtLabelS"]    = L"Spi:",
  ["txtIcon"]      = L"<icon=155>",
  ["txtPowerIcon"] = L"<icon=155>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            Statdoll.Stats.formatResists(statArray)
           end,
}

-- Ele Resist
Statdoll.Stats.values[Statdoll._ELEMENTAL] = {
  ["refWnd"]       = "ElementalResist",
  ["lookupStr"]    = "ElementalResistance",
  
  ["txtLabel"]     = L"Elemental",
  ["txtLabelS"]    = L"Ele:",
  ["txtIcon"]      = L"<icon=162>",
  ["txtPowerIcon"] = L"<icon=162>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            Statdoll.Stats.formatResists(statArray)
           end,
}

-- Corporeal Resist
Statdoll.Stats.values[Statdoll._CORPOREAL] = {
  ["refWnd"]       = "CorpResist",
  ["lookupStr"]    = "CorporealResistance",
  
  ["txtLabel"]     = L"Corporeal",
  ["txtLabelS"]    = L"Cor:",
  ["txtIcon"]      = L"<icon=164>",
  ["txtPowerIcon"] = L"<icon=164>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            Statdoll.Stats.formatResists(statArray)
           end,
}

-- Block Chance
Statdoll.Stats.values[Statdoll._BLOCK] = {
  ["refWnd"]       = "Block",
  ["lookupStr"]    = "Block",
  
  ["txtLabel"]     = L"Block",
  ["txtLabelS"]    = L"Blk:",
  ["txtIcon"]      = L"<icon=165>",
  ["txtPowerIcon"] = L"<icon=165>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function (statArray)
              statArray["txtValue"] = math.floor(statArray["value"][1] + statArray["value"][2])..L"%"
              statArray["txtPowerValue"] = math.floor(statArray["value"][1]) .. L"+" .. math.floor(statArray["value"][2]) .. L"%" 
           end,
}

-- Parry Chance
Statdoll.Stats.values[Statdoll._PARRY] = {
  ["refWnd"]       = "Parry",
  ["lookupStr"]    = "Parry",
  
  ["txtLabel"]     = L"Parry",
  ["txtLabelS"]    = L"Par:",
  ["txtIcon"]      = L"<icon=110>",
  ["txtPowerIcon"] = L"<icon=106>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function (statArray)
              statArray["txtValue"] = math.floor(statArray["value"][1] + statArray["value"][2] + Statdoll.Stats.dualParry)..L"%"
              statArray["txtPowerValue"] = math.floor(statArray["value"][1] + Statdoll.Stats.dualParry) .. L"+" .. math.floor(statArray["value"][2]) .. L"%" 
           end,
}

-- Dodge Chance
Statdoll.Stats.values[Statdoll._DODGE] = {
  ["refWnd"]       = "Dodge",
  ["lookupStr"]    = "Evade",
  
  ["txtLabel"]     = L"Dodge",
  ["txtLabelS"]    = L"Dod:",
  ["txtIcon"]      = L"<icon=111>",
  ["txtPowerIcon"] = L"<icon=105>",
  
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function (statArray)
              statArray["txtValue"] = math.floor(statArray["value"][1] + statArray["value"][2])..L"%"
              statArray["txtPowerValue"] = math.floor(statArray["value"][1]) .. L"+" .. math.floor(statArray["value"][2]) .. L"%" 
           end,
}

-- Disrupt
Statdoll.Stats.values[Statdoll._DISRUPT] = {
  ["refWnd"]       = "Disrupt",
  ["lookupStr"]    = "Disrupt",
  
  ["txtLabel"]     = L"Disrupt",
  ["txtLabelS"]    = L"Dis:",
  ["txtIcon"]      = L"<icon=112>",
  ["txtPowerIcon"] = L"<icon=102>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format =  function (statArray)
              statArray["txtValue"] = math.floor(statArray["value"][1] + statArray["value"][2])..L"%"
              statArray["txtPowerValue"] = math.floor(statArray["value"][1]) .. L"+" .. math.floor(statArray["value"][2]) .. L"%" 
            end,
}

-- Healing Power
Statdoll.Stats.values[Statdoll._HEALPOWER] = {
  ["refWnd"]       = "HealPower",
  ["lookupStr"]    = "HealingPower",
  
  ["txtLabel"]     = L"Healing Power",
  ["txtLabelS"]    = L"Pow:",
  ["txtIcon"]      = L"<icon=156>",
  ["txtPowerIcon"] = L"<icon=160>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            --statArray["txtValue"] = L"+" .. math.floor(statArray["value"][1] / 5)
            statArray["txtPowerValue"] = math.floor((statArray["value"][1] + statArray["value"][2]) / 5)
           end,
}

-- Healing Crit Chance
Statdoll.Stats.values[Statdoll._HEALCRIT] = {
  ["refWnd"]       = "HealPower",
  ["refWndX"]      = "Crit",
  ["lookupStr"]    = "HealCritRate",
  
  ["txtLabel"]     = L"",
  ["txtLabelS"]    = L"Crit:",
  ["txtIcon"]      = L"<icon=163>",
  ["txtPowerIcon"] = L"<icon=163>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtValue"] = towstring(statArray["value"][1]) .. L"%"
            statArray["txtPowerValue"] = towstring(statArray["value"][1]) .. L"%"
           end,
}

-- Magic Power
Statdoll.Stats.values[Statdoll._MAGICPOWER] = {
  ["refWnd"]       = "MagicPower",
  ["lookupStr"]    = "MagicPower",
  
  ["txtLabel"]     = L"Magic Power",
  ["txtLabelS"]    = L"Pow:",
  ["txtIcon"]      = L"<icon=156>",
  ["txtPowerIcon"] = L"<icon=156>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            --statArray["txtValue"] = L"+" .. math.floor(statArray["value"][1] / 5)
            statArray["txtPowerValue"] = math.floor((statArray["value"][1] + statArray["value"][2]) / 5)
           end,
}

-- Magic Crit Chance
Statdoll.Stats.values[Statdoll._MAGICCRIT] = {
  ["refWnd"]       = "MagicPower",
  ["refWndX"]      = "Crit",
  ["lookupStr"]    = "MagicCritRate",
  
  ["txtLabel"]     = L"",
  ["txtLabelS"]    = L"Crit:",
  ["txtIcon"]      = L"<icon=163>",
  ["txtPowerIcon"] = L"<icon=163>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtValue"] = towstring(statArray["value"][1]) .. L"%"
            statArray["txtPowerValue"] = towstring(statArray["value"][1]) .. L"%"
           end,
}

-- Range Power
Statdoll.Stats.values[Statdoll._RANGEPOWER] = {
  ["refWnd"]       = "RangePower",
  ["lookupStr"]    = "RangedPower",
  
  ["txtLabel"]     = L"Range Power",
  ["txtLabelS"]    = L"Pow:",
  ["txtIcon"]      = L"<icon=156>",
  ["txtPowerIcon"] = L"<icon=157>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            --statArray["txtValue"] = L"+" .. math.floor(statArray["value"][1] / 5)
            statArray["txtPowerValue"] = math.floor((statArray["value"][1] + statArray["value"][2]) / 5)
           end,
}

-- Range Crit Chance
Statdoll.Stats.values[Statdoll._RANGECRIT] = {
  ["refWnd"]       = "RangePower",
  ["refWndX"]      = "Crit",
  ["lookupStr"]    = "RangedCritRate",
  
  ["txtLabel"]     = L"",
  ["txtLabelS"]    = L"Crit:",
  ["txtIcon"]      = L"<icon=163>",
  ["txtPowerIcon"] = L"<icon=163>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {15, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtValue"] = towstring(statArray["value"][1]) .. L"%"
            statArray["txtPowerValue"] = towstring(statArray["value"][1]) .. L"%"
           end,
}

-- Range Attack Power
Statdoll.Stats.values[Statdoll._RANGEAAPOWER] = {
  ["refWnd"]       = "RangeAAPower",
  
  ["txtLabel"]     = L"Range AA/Sec",
  ["txtLabelS"]    = L"",
  ["txtIcon"]      = L"<icon=107>",
  ["txtPowerIcon"] = L"<icon=157>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtValue"] = L"+" .. math.floor(statArray["value"][1] / 10 * Statdoll.Stats.speedRangeWp)
            statArray["txtPowerValue"] = math.floor(((statArray["value"][1] / 10) + statArray["value"][2]) * Statdoll.Stats.speedRangeWp)
           end,
}

-- Range Dps
Statdoll.Stats.values[Statdoll._RANGEDPS] = {
  ["refWnd"]       = "RangeAAPower",
  ["refWndX"]      = "Crit",
  
  ["txtLabel"]     = L"",
  ["txtLabelS"]    = L"",
  ["txtIcon"]      = L"<icon=111>",
  ["txtPowerIcon"] = L"",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {15, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
             statArray["txtValue"] = wstring.format(L"%.01f", statArray["value"][1])
           end
}

-- Melee Power
Statdoll.Stats.values[Statdoll._MELEEPOWER] = {
  ["refWnd"]       = "MeleePower",
  ["lookupStr"]    = "MeleePower",
  
  ["txtLabel"]     = L"Melee Power",
  ["txtLabelS"]    = L"Pow:",
  ["txtIcon"]      = L"<icon=156>",
  ["txtPowerIcon"] = L"<icon=158>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            --statArray["txtValue"] = L"+" .. math.floor(statArray["value"][1] / 5)
            statArray["txtPowerValue"] = math.floor((statArray["value"][1] + statArray["value"][2]) / 5)
           end,
}

-- Melee Crit Chance
Statdoll.Stats.values[Statdoll._MELEECRIT] = {
  ["refWnd"]       = "MeleePower",
  ["refWndX"]      = "Crit",
  ["lookupStr"]    = "MeleeCritRate",
  
  ["txtLabel"]     = L"",
  ["txtLabelS"]    = L"Crit:",
  ["txtIcon"]      = L"<icon=163>",
  ["txtPowerIcon"] = L"<icon=163>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {15, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtValue"] = towstring(statArray["value"][1]) .. L"%"
            statArray["txtPowerValue"] = towstring(statArray["value"][1]) .. L"%"
           end,
}

-- Melee AA/S
Statdoll.Stats.values[Statdoll._MELEEAAPOWER] = {
  ["refWnd"]       = "MeleeAAPower",
  
  ["txtLabel"]     = L"Melee AA/Sec",
  ["txtLabelS"]    = L"",
  ["txtIcon"]      = L"<icon=100>",
  ["txtPowerIcon"] = L"<icon=159>",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {0, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
            statArray["txtValue"] = L"+" .. math.floor(statArray["value"][1] / 10 * Statdoll.Stats.speedMeleeWp * Statdoll.Stats.dualWield)
            statArray["txtPowerValue"] = math.floor((statArray["value"][2] + Statdoll.Stats.offhand + (statArray["value"][1] / 10 * Statdoll.Stats.dualWield)) * Statdoll.Stats.speedMeleeWp)
           end,
}

-- Melee DPS
Statdoll.Stats.values[Statdoll._MELEEDPS] = {
  ["refWnd"]       = "MeleeAAPower",
  ["refWndX"]      = "Crit",
  
  ["txtLabel"]     = L"",
  ["txtLabelS"]    = L"",
  ["txtIcon"]      = L"<icon=111>",
  ["txtPowerIcon"] = L"",
  
  ["color-r"]      = 255,
  ["color-g"]      = 255,
  ["color-b"]      = 255,
  
  ["refValue"]      = {0, 0},
  ["value"]         = {15, 0},
  ["txtValue"]      = L"",
  ["txtPowerValue"] = L"",
  
  format = function(statArray)
             statArray["txtValue"] = wstring.format(L"%.01f", statArray["value"][1])
           end
}