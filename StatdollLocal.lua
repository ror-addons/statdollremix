if (Statdoll == nil) then
  Statdoll = {}
end

------------------------------
-- Statdoll.Getstats
--
Statdoll.Local = {
}

------------------------------
-- Locals
--
local eventsRegistered = false
local delayedUpdate    = false

-- Statdoll.Local.onInitialize
function Statdoll.Local.onInitialize()
end

-- Statdoll.Local.onShutdown
function Statdoll.Local.onShutdown()
  Statdoll.Local.unregisterEventHandler()
end

-- Statdoll.Local.onUpdate
function Statdoll.Local.onUpdate()
  -- get current values
  Statdoll.Local.setCurrentValues();
  
  -- force setting the reference values
  if (Statdoll.Stats.refValuesForce == true) then
    Statdoll.Local.setReferenceValues()
  end
  
  -- notify Statdoll that stats are updated
  Statdoll.Stats.onStatsUpdated()
end

-- Statdoll.Local.delayedUpdate
function Statdoll.Local.delayedUpdate()
  if(delayedUpdate == false) then return end

  UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED,  "Statdoll.Local.delayedUpdate")
  delayedUpdate = false
  
  Statdoll.Local.onUpdate()  
end

-- Statdoll.Local.queueUpdate
function Statdoll.Local.queueUpdate()
  if(delayedUpdate == true) then return end

  RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED,  "Statdoll.Local.delayedUpdate")
  delayedUpdate = true
end


-- Statdoll.Local.registerEventHandler
function Statdoll.Local.registerEventHandler()
  if (eventsRegistered == true) then return end

  RegisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED,  "Statdoll.Local.onUpdate")
  RegisterEventHandler(SystemData.Events.PLAYER_ACTIVE_TACTICS_UPDATED,  "Statdoll.Local.onUpdate")
  RegisterEventHandler(SystemData.Events.PLAYER_STATS_UPDATED,  "Statdoll.Local.onUpdate")
  
  RegisterEventHandler(SystemData.Events.PLAYER_EQUIPMENT_SLOT_UPDATED,  "Statdoll.Local.queueUpdate")
  
  eventsRegistered = true
end

-- Statdoll.Local.unregisterEventHandler
function Statdoll.Local.unregisterEventHandler()
  if (eventsRegistered == false) then return end

  UnregisterEventHandler(SystemData.Events.PLAYER_STATS_UPDATED,  "Statdoll.Local.onUpdate")
  UnregisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED,  "Statdoll.Local.onUpdate")
  UnregisterEventHandler(SystemData.Events.PLAYER_ACTIVE_TACTICS_UPDATED,  "Statdoll.Local.onUpdate")
  UnregisterEventHandler(SystemData.Events.PLAYER_EQUIPMENT_SLOT_UPDATED,  "Statdoll.Local.onUpdate")
  
  eventsRegistered = false
end

-- Statdoll.Local.onWindowShown
function Statdoll.Local.onWindowShown()
  Statdoll.Local.registerEventHandler()
  Statdoll.Local.onUpdate()
end

-- Statdoll.Local.onWindowHidden
function Statdoll.Local.onWindowHidden()
  Statdoll.Local.unregisterEventHandler()
end

-- Statdoll.Local.forceUpdate
function Statdoll.Local.forceUpdate()
  Statdoll.Local.onUpdate()
end

-- Statdoll.Local.setReferenceValues
function Statdoll.Local.setReferenceValues()
  for _, statArray in pairs(Statdoll.Stats.values) do
    statArray["refValue"][1] = statArray["value"][1]
    statArray["refValue"][2] = statArray["value"][2]
  end

  -- check if reference values were set successfully
  if (Statdoll.Stats.values[Statdoll._STRENGTH]["refValue"][1] > 0) then
    Statdoll.Stats.refValuesSet   = true
    Statdoll.Stats.refValuesForce = false
  end
end

-- Statdoll.Local.setCurrentValues
function Statdoll.Local.setCurrentValues()
  Statdoll.Stats.speedMeleeWp = Statdoll.Stats.showMeleeDPS == true
    and 1
    or CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].speed

  Statdoll.Stats.speedRangeWp = Statdoll.Stats.showRangeDPS == true
    and 1
    or CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].speed

  Statdoll.Stats.offhand = (CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps) * 0.375

  Statdoll.Stats.dualParry = 0
  Statdoll.Stats.dualWield = 1
  if (CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps > 0) and
    (CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps > 0) then

    Statdoll.Stats.dualParry = 10
    Statdoll.Stats.dualWield = 1.5
  end

  Statdoll.Local.getLocalStats("value")
end

-- Statdoll.Local.getLocalStats
function Statdoll.Local.getLocalStats(windex)
  Statdoll.Stats.values[Statdoll._STRENGTH][windex][1]     = GetBonus(GameData.BonusTypes.EBONUS_STRENGTH, GameData.Player.Stats[GameData.Stats.STRENGTH].baseValue)
  Statdoll.Stats.values[Statdoll._BALLISTIC][windex][1]    = GetBonus(GameData.BonusTypes.EBONUS_BALLISTICSKILL, GameData.Player.Stats[GameData.Stats.BALLISTICSKILL].baseValue)
  Statdoll.Stats.values[Statdoll._INTELLIGENCE][windex][1] = GetBonus(GameData.BonusTypes.EBONUS_INTELLIGENCE, GameData.Player.Stats[GameData.Stats.INTELLIGENCE].baseValue)
  Statdoll.Stats.values[Statdoll._WILLPOWER][windex][1]    = GetBonus(GameData.BonusTypes.EBONUS_WILLPOWER, GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue)
  Statdoll.Stats.values[Statdoll._WEAPONSKILL][windex][1]  = GetBonus(GameData.BonusTypes.EBONUS_WEAPONSKILL, GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue)
  Statdoll.Stats.values[Statdoll._INITIATIVE][windex][1]   = GetBonus(GameData.BonusTypes.EBONUS_INITIATIVE, GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue)
  Statdoll.Stats.values[Statdoll._TOUGHNESS][windex][1]    = GetBonus(GameData.BonusTypes.EBONUS_TOUGHNESS, GameData.Player.Stats[GameData.Stats.TOUGHNESS].baseValue)
  Statdoll.Stats.values[Statdoll._WOUNDS][windex][1]       = GetBonus(GameData.BonusTypes.EBONUS_WOUNDS, GameData.Player.Stats[GameData.Stats.WOUNDS].baseValue)
  Statdoll.Stats.values[Statdoll._ARMOR][windex][1]        = GameData.Player.armorValue
  Statdoll.Stats.values[Statdoll._SPIRITUAL][windex][1]    = GetBonus(GameData.BonusTypes.EBONUS_SPIRIT_RESIST, GameData.Player.Stats[GameData.Stats.SPIRITRESIST].baseValue)
  Statdoll.Stats.values[Statdoll._ELEMENTAL][windex][1]    = GetBonus(GameData.BonusTypes.EBONUS_ELEMENTAL_RESIST, GameData.Player.Stats[GameData.Stats.ELEMENTALRESIST].baseValue)
  Statdoll.Stats.values[Statdoll._CORPOREAL][windex][1]    = GetBonus(GameData.BonusTypes.EBONUS_CORPOREAL_RESIST, GameData.Player.Stats[GameData.Stats.CORPOREALRESIST].baseValue)

  -- block chance
  Statdoll.Stats.values[Statdoll._BLOCK][windex][1] = GetBonus(GameData.BonusTypes.EBONUS_BLOCK, 0)
  Statdoll.Stats.values[Statdoll._BLOCK][windex][2] = CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].blockRating / (Statdoll.Stats.playerLevel * 7.5 + 50) * 5

  -- parry chance
  Statdoll.Stats.values[Statdoll._PARRY][windex][1] = GetBonus(GameData.BonusTypes.EBONUS_PARRY, 0)
  Statdoll.Stats.values[Statdoll._PARRY][windex][2] = Statdoll.Stats.values[Statdoll._WEAPONSKILL][windex][1] / (Statdoll.Stats.playerLevel * 7.5 + 50) * 13.5

  -- dodge chance
  Statdoll.Stats.values[Statdoll._DODGE][windex][1] = GetBonus(GameData.BonusTypes.EBONUS_EVADE, 0)
  Statdoll.Stats.values[Statdoll._DODGE][windex][2] = Statdoll.Stats.values[Statdoll._INITIATIVE][windex][1] / (Statdoll.Stats.playerLevel * 7.5 + 50 ) * 13.5

  -- disrupt
  Statdoll.Stats.values[Statdoll._DISRUPT][windex][1] = GetBonus(GameData.BonusTypes.EBONUS_DISRUPT, 0)
  Statdoll.Stats.values[Statdoll._DISRUPT][windex][2] = Statdoll.Stats.values[Statdoll._WILLPOWER][windex][1] / (Statdoll.Stats.playerLevel * 7.5 + 50) * 13.5

  -- healing stats
  Statdoll.Stats.values[Statdoll._HEALPOWER][windex][1] = GetBonus(GameData.BonusTypes.EBONUS_HEALING_POWER, 0)
  Statdoll.Stats.values[Statdoll._HEALPOWER][windex][2] = Statdoll.Stats.values[Statdoll._WILLPOWER][windex][1]
  Statdoll.Stats.values[Statdoll._HEALCRIT][windex][1]  = GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_HEALING, 0)

  -- magic stats
  Statdoll.Stats.values[Statdoll._MAGICPOWER][windex][1] = GetBonus(GameData.BonusTypes.EBONUS_DAMAGE_MAGIC, 0)
  Statdoll.Stats.values[Statdoll._MAGICPOWER][windex][2] = Statdoll.Stats.values[Statdoll._INTELLIGENCE][windex][1]
  Statdoll.Stats.values[Statdoll._MAGICCRIT][windex][1]  = GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_MAGIC, 0)

  -- Range stats --
  Statdoll.Stats.values[Statdoll._RANGEPOWER][windex][1]   = GetBonus(GameData.BonusTypes.EBONUS_DAMAGE_RANGED, 0)
  Statdoll.Stats.values[Statdoll._RANGEPOWER][windex][2]   = Statdoll.Stats.values[Statdoll._BALLISTIC][windex][1]
  Statdoll.Stats.values[Statdoll._RANGECRIT][windex][1]    = GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_RANGED, 0)
  Statdoll.Stats.values[Statdoll._RANGEAAPOWER][windex][1] = Statdoll.Stats.values[Statdoll._BALLISTIC][windex][1]
  Statdoll.Stats.values[Statdoll._RANGEAAPOWER][windex][2] = CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].dps
  Statdoll.Stats.values[Statdoll._RANGEDPS][windex][1]     = CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].speed

  -- Melee stats --
  Statdoll.Stats.values[Statdoll._MELEEPOWER][windex][1]   = GetBonus(GameData.BonusTypes.EBONUS_DAMAGE_MELEE, 0)
  Statdoll.Stats.values[Statdoll._MELEEPOWER][windex][2]   = Statdoll.Stats.values[Statdoll._STRENGTH][windex][1]
  Statdoll.Stats.values[Statdoll._MELEECRIT][windex][1]    = GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_MELEE, 0)
  Statdoll.Stats.values[Statdoll._MELEEAAPOWER][windex][1] = Statdoll.Stats.values[Statdoll._STRENGTH][windex][1]
  Statdoll.Stats.values[Statdoll._MELEEAAPOWER][windex][2] = CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps
  Statdoll.Stats.values[Statdoll._MELEEDPS][windex][1]     = CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].speed
end