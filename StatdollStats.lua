if (Statdoll == nil) then
  Statdoll = {}
end

if (Statdoll.Stats == nil) then
  Statdoll.Stats = {}
end

------------------------------
-- Statdoll.Stats
--
Statdoll.Stats = {
  listener       = nil,
  refValuesForce = true,
  refValuesSet   = false,

  playerLevel  = GameData.Player.battleLevel,
  statCap      = GameData.Player.battleLevel * 25 + 50,
  offhand      = 0,
  dualParry    = 0,
  dualWield    = 1,

  showMeleeDPS  = true,
  speedMeleeWp  = 1,

  showRangeDPS = true,
  speedRangeWp = 1,
}

------------------------------
-- Locals
--
local c_MODE_LOCAL    = 1
local c_MODE_GETSTATS = 2

-- Statdoll.Stats.onInitialize:
function Statdoll.Stats.onInitialize()
  -- reset any previous stats to zero
  Statdoll.Stats.reset()
  
  -- initialize stat listener
  Statdoll.Stats.listener = Statdoll.settings["stats-mode"] == c_MODE_GETSTATS
    and Statdoll.Getstats
    or  Statdoll.Local
  
  Statdoll.Stats.listener.onInitialize()
  
  if(Statdoll.fullyLoaded == true) then
    Statdoll.Stats.listener.forceUpdate()
  end
end

-- Statdoll.Stats.onShutdown
function Statdoll.Stats.onShutdown()
  --  unregister event handlers
  Statdoll.Stats.listener.onShutdown()
  Statdoll.Stats.listener = nil
end

-- Statdoll.Stats.onStatsUpdated
function Statdoll.Stats.onStatsUpdated()
  -- update formatting for stats and additional stats
  for i, statArray in pairs(Statdoll.Stats.values) do
    Statdoll.Stats.format(statArray)
  end
  
  for i, statArray in pairs(Statdoll.Stats.additionalValues) do
    Statdoll.Stats.format(statArray)
  end
  
  -- update window
  Statdoll.statdollWnd.onUpdate()
end

-- Statdoll.Stats.reset
function Statdoll.Stats.reset()
  Statdoll.Stats.refValuesForce = true
  Statdoll.Stats.refValuesSet   = false
  
  Statdoll.Stats.offhand      = 0
  Statdoll.Stats.dualParry    = 0
  Statdoll.Stats.dualWield    = 1

  for i, statArray in pairs(Statdoll.Stats.values) do
    statArray["refValue"] = {0, 0}
    statArray["value"][1] = 0
    statArray["value"][2] = 0
    statArray["txtValue"] = L"0"
    statArray["txtPowerValue"] = L"0"
  end
  
  Statdoll.Stats.additionalValues = {}
end

-- Statdoll.Stats.format
function Statdoll.Stats.format(statArray)
  statArray["txtValue"] = statArray["value"][1]
  
  -- default, no change, set white
  statArray["color-r"] = 254
  statArray["color-g"] = 254
  statArray["color-b"] = 254

  -- recolor values only if reference values were set
  if (Statdoll.Stats.refValuesSet == true) then
    -- stat buffed, set green
    if math.floor(statArray["value"][1]+statArray["value"][2]) > math.floor(statArray["refValue"][1]+statArray["refValue"][2]) then
      statArray["color-r"] = 0
      statArray["color-g"] = 254
      statArray["color-b"] = 0
    end

    -- stat debuffed, set red
    if math.floor(statArray["value"][1]+statArray["value"][2]) < math.floor(statArray["refValue"][1]+statArray["refValue"][2]) then
      statArray["color-r"] = 254
      statArray["color-g"] = 0
      statArray["color-b"] = 0
    end
  end
  
  if (statArray.format ~= nil) then
    statArray.format(statArray)
  end
end

-- Statdoll.Stats.formatBaseStat
function Statdoll.Stats.formatBaseStat(statArray)
  -- stat softcapped, calculate diminishing returns and set orange
  if statArray["value"][1] > Statdoll.Stats.statCap then
    statArray["value"][1] = math.floor((statArray["value"][1] - Statdoll.Stats.statCap) / 2 + Statdoll.Stats.statCap )
    statArray["color-r"] = 254
    statArray["color-g"] = 128
    statArray["color-b"] = 0
  end
end

-- Statdoll.Stats.formatResists
function Statdoll.Stats.formatResists(statArray)
  statArray["txtPowerValue"] = wstring.format(L"%.01f", statArray["value"][1] / (Statdoll.Stats.playerLevel * 0.42))..L"%"

  -- softcapped, calculate diminishing returns and set orange
  if (statArray["value"][1] / (Statdoll.Stats.playerLevel * 0.42)) > 40 then
    statArray["txtPowerValue"] = wstring.format(L"%.01f", statArray["value"][1] / (Statdoll.Stats.playerLevel * 1.2604) + 26.6)..L"%"
    statArray["color-r"] = 254
    statArray["color-g"] = 128
    statArray["color-b"] = 0
  end
end

-- Statdoll.Stats.setRangeDPS
function Statdoll.Stats.setRangeDPS(showDPS)
  Statdoll.Stats.showRangeDPS = showDPS or not Statdoll.Stats.showRangeDPS

  Statdoll.Stats.values[Statdoll._RANGEAAPOWER]["txtLabel"] = Statdoll.Stats.showRangeDPS
    and L"Range AA/Sec"
    or  L"Range AA/Hit"
end

-- Statdoll.Stats.setMeleeDPS
function Statdoll.Stats.setMeleeDPS(showDPS)
  Statdoll.Stats.showMeleeDPS = showDPS or not Statdoll.Stats.showMeleeDPS

  Statdoll.Stats.values[Statdoll._MELEEAAPOWER]["txtLabel"] = Statdoll.Stats.showMeleeDPS
    and L"Melee AA/Sec"
    or  L"Melee AA/Hit"
end


