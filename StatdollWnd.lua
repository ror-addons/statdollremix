if( StatdollWnd == nil ) then
  StatdollWnd = {}
end

------------------------------
-- StatdollWnd
--
StatdollWnd = {
  window = "StatdollWnd",
  windowMagic = "StatdollMagicWnd",
  windowRange = "StatdollRangeWnd",
  windowMelee = "StatdollMeleeWnd",
  defaultWidth  = 340,
  defaultHeight = 420,
}

function StatdollWnd.onInitialize()
  -- create windows
  CreateWindow(StatdollWnd.windowMelee, false)
  CreateWindow(StatdollWnd.windowRange, false)
  CreateWindow(StatdollWnd.windowMagic, false)
  CreateWindow(StatdollWnd.window, false)

  -- reposition / resize windows
  WindowAddAnchor(StatdollWnd.window, "topleft", "Root", "topleft",
    Statdoll.settings["ui-pos-x"],
    Statdoll.settings["ui-pos-y"])

  WindowAddAnchor(StatdollWnd.windowMagic, "bottom", StatdollWnd.window .. "Background", "top", 0, 5)
  WindowAddAnchor(StatdollWnd.windowRange, "bottom", StatdollWnd.windowMagic, "top", 0, 0)
  WindowAddAnchor(StatdollWnd.windowMelee, "bottom", StatdollWnd.windowRange, "top", 0, 0)

  if (Statdoll.settings["ui-show-magic"] == false) then
    WindowSetDimensions(StatdollWnd.windowMagic, 0,0)
  end

  if (Statdoll.settings["ui-show-range"] == false) then
    WindowSetDimensions(StatdollWnd.windowRange, 0,0)
  end

  if (Statdoll.settings["ui-show-melee"] == false) then
    WindowSetDimensions(StatdollWnd.windowMelee, 0,0)
  end

  -- set labels and icons
  StatdollWnd.setLabels()
end

function StatdollWnd.onShutdown()
  -- save current window position
  Statdoll.settings["ui-pos-x"], Statdoll.settings["ui-pos-y"] = WindowGetOffsetFromParent(StatdollWnd.window)

  -- destroy all windows
  DestroyWindow(StatdollWnd.windowMelee)
  DestroyWindow(StatdollWnd.windowRange)
  DestroyWindow(StatdollWnd.windowMagic)
  DestroyWindow(StatdollWnd.window)
end

function StatdollWnd.onUpdate()
  for i, value in pairs(Statdoll.Stats.values) do
    local refWnd = value["refWnd"]
    if (value["refWndX"] ~= nil and value["refWndX"] ~= "") then
      refWnd = refWnd .. value["refWndX"]
    end

    if (DoesWindowExist(refWnd .. "Value")) then
      LabelSetText(refWnd .. "Value", towstring(value["txtValue"]))
      LabelSetTextColor(refWnd .. "Value", value["color-r"], value["color-g"], value["color-b"])
    end

    if (DoesWindowExist(refWnd .. "PowerValue")) then
      LabelSetText(refWnd .. "PowerValue", towstring(value["txtPowerValue"]))
      LabelSetTextColor(refWnd .. "PowerValue", value["color-r"], value["color-g"], value["color-b"])
    end
  end

  -- destroy previous window, if it exists
  if(DoesWindowExist("StatdollWndAddValues")) then
    DestroyWindow("StatdollWndAddValues")
  end

  -- resize statdoll window
  local y = (#Statdoll.Stats.additionalValues * 21) + 12
  WindowSetDimensions(StatdollWnd.window, StatdollWnd.defaultWidth, (StatdollWnd.defaultHeight + y))

  -- check for additional values from getstat
  if(#Statdoll.Stats.additionalValues > 0) then
    -- create window
    CreateWindowFromTemplate("StatdollWndAddValues", "StatdollWndAddValuesTemplate", StatdollWnd.window)
    WindowSetDimensions("StatdollWndAddValues", 300, y)

    local lastAnchor = "StatdollWndAddValuesSeparator"

    -- add row for each additional value
    for i, statArray in pairs(Statdoll.Stats.additionalValues) do
      -- create stats row
      CreateWindowFromTemplate(statArray["refWnd"], "StatdollWndAddValueRowTemplate", "StatdollWndAddValues")
      WindowAddAnchor(statArray["refWnd"], "bottom", lastAnchor, "top", 0, 0)

      lastAnchor = statArray["refWnd"]

      -- set label and icons
      LabelSetText(statArray["refWnd"] .. "Icon", towstring(statArray["txtIcon"]))
      LabelSetText(statArray["refWnd"] .. "Label", towstring(statArray["txtLabel"]))
      LabelSetText(statArray["refWnd"] .. "PowerIcon", L"")

      -- set value and color
      LabelSetText(statArray["refWnd"] .. "Value", towstring(statArray["txtValue"]))
      LabelSetTextColor(statArray["refWnd"] .. "Value", statArray["color-r"], statArray["color-g"], statArray["color-b"])
      LabelSetText(statArray["refWnd"] .. "PowerValue", L"")
    end
  end
end

function StatdollWnd.setLabels()
  -- Buttons
  LabelSetText("ButtonMagicIcon", L"<icon=156>")
  LabelSetText("ButtonRangeIcon", L"<icon=157>")
  LabelSetText("ButtonMeleeIcon", L"<icon=111>")

  for i, value in pairs(Statdoll.Stats.values) do
    local refWnd = value["refWnd"]
    if (value["refWndX"] ~= nil and value["refWndX"] ~= "") then
      refWnd = refWnd .. value["refWndX"]
    end

    if (DoesWindowExist(refWnd .. "Label")) then
      LabelSetText(refWnd .. "Label", towstring(value["txtLabel"]))
    end

    if (DoesWindowExist(refWnd .. "Icon")) then
      LabelSetText(refWnd .. "Icon", towstring(value["txtIcon"]))
    end

    if (DoesWindowExist(refWnd .. "PowerIcon")) then
      LabelSetText(refWnd .. "PowerIcon", towstring(value["txtPowerIcon"]))
    end

    if (DoesWindowExist(refWnd .. "Value")) then
      LabelSetText(refWnd .. "Value", L"0")
      LabelSetTextColor(refWnd .. "Value", value["color-r"], value["color-g"], value["color-b"])
    end

    if (DoesWindowExist(refWnd .. "PowerValue")) then
      LabelSetText(refWnd .. "PowerValue", L"0")
      LabelSetTextColor(refWnd .. "PowerValue", value["color-r"], value["color-g"], value["color-b"])
    end
  end
end

function StatdollWnd.setVisible(visible)
  if(visible == true) then
    WindowSetShowing(StatdollWnd.window, true)
    WindowSetShowing(StatdollWnd.windowMagic, Statdoll.settings["ui-show-magic"])
    WindowSetShowing(StatdollWnd.windowRange, Statdoll.settings["ui-show-range"])
    WindowSetShowing(StatdollWnd.windowMelee, Statdoll.settings["ui-show-melee"])
  else
    WindowSetShowing(StatdollWnd.window, false)
    WindowSetShowing(StatdollWnd.windowMagic, false)
    WindowSetShowing(StatdollWnd.windowRange, false)
    WindowSetShowing(StatdollWnd.windowMelee, false)
  end
end

function StatdollWnd.setScaleFactor(scale)
  scale = scale * WindowGetScale("Root")
  WindowSetScale(StatdollWnd.window, scale)
  WindowSetScale(StatdollWnd.windowMagic, scale)
  WindowSetScale(StatdollWnd.windowRange, scale)
  WindowSetScale(StatdollWnd.windowMelee, scale)
end

function StatdollWnd.setCloseButtonVisible(visible)
  WindowSetShowing(StatdollWnd.window .. "CloseButton", visible)
end

function StatdollWnd.setBackground(visible)
  WindowSetShowing(StatdollWnd.window .. "Background", visible)
  WindowSetShowing(StatdollWnd.windowMagic .. "Background", visible)
  WindowSetShowing(StatdollWnd.windowRange .. "Background", visible)
  WindowSetShowing(StatdollWnd.windowMelee .. "Background", visible)
end

function StatdollWnd.setLocked(locked)
  if (locked == true) then
    WindowSetMovable(StatdollWnd.window, false)
    WindowSetHandleInput(StatdollWnd.window, false)
    WindowSetHandleInput(StatdollWnd.windowMagic, false)
    WindowSetHandleInput(StatdollWnd.windowRange, false)
    WindowSetHandleInput(StatdollWnd.windowMelee, false)
  else
    WindowSetMovable(StatdollWnd.window, true)
    WindowSetHandleInput(StatdollWnd.window, true)
    WindowSetHandleInput(StatdollWnd.windowMagic, true)
    WindowSetHandleInput(StatdollWnd.windowRange, true)
    WindowSetHandleInput(StatdollWnd.windowMelee, true)
  end
end

function StatdollWnd.setBackgroundAlpha(alpha)
  WindowSetAlpha(StatdollWnd.window .. "Background", alpha)
  WindowSetAlpha(StatdollWnd.windowMagic .. "Background", alpha)
  WindowSetAlpha(StatdollWnd.windowRange .. "Background", alpha)
  WindowSetAlpha(StatdollWnd.windowMelee .. "Background", alpha)
end

function StatdollWnd.onLBMagicWnd()
  if (WindowGetShowing(StatdollWnd.windowMagic)) then
    WindowSetDimensions(StatdollWnd.windowMagic, 0,0)
    WindowSetShowing(StatdollWnd.windowMagic, false)
    Statdoll.settings["ui-show-magic"] = false
  else
    WindowSetShowing(StatdollWnd.windowMagic, true)
    WindowSetDimensions(StatdollWnd.windowMagic, 330,90)
    Statdoll.settings["ui-show-magic"] = true
  end
end

function StatdollWnd.onLBRangeWnd()
  if (WindowGetShowing(StatdollWnd.windowRange)) then
    WindowSetDimensions(StatdollWnd.windowRange, 0,0)
    WindowSetShowing(StatdollWnd.windowRange, false)
    Statdoll.settings["ui-show-range"] = false
  else
    WindowSetShowing(StatdollWnd.windowRange, true)
    WindowSetDimensions(StatdollWnd.windowRange, 330,90)
    Statdoll.settings["ui-show-range"] = true
  end
end

function StatdollWnd.onLBMeleeWnd()
  if (WindowGetShowing(StatdollWnd.windowMelee)) then
    WindowSetDimensions(StatdollWnd.windowMelee, 0,0)
    WindowSetShowing(StatdollWnd.windowMelee, false)
    Statdoll.settings["ui-show-melee"] = false
  else
    WindowSetShowing(StatdollWnd.windowMelee, true)
    WindowSetDimensions(StatdollWnd.windowMelee, 330,90)
    Statdoll.settings["ui-show-melee"] = true
  end
end
