if (Statdoll == nil) then
  Statdoll = {}
end

--------
-- TODO:
-- * Stats are wrong after a level up

------------------------------
-- Constants
--
local c_VERSION_MAJOR = 0
local c_VERSION_MINOR = 9
local c_VERSION_PATCH = 0
local c_UI_FULL       = 1
local c_UI_LIGHT      = 2
local c_MODE_LOCAL    = 1
local c_MODE_GETSTATS = 2

------------------------------
-- Statdoll
--
Statdoll = {
  statdollWnd = nil,
  optionsWnd  = nil,
  fullyLoaded = false,
}

Statdoll.defaultSettings = {
  ["version-major"]   = c_VERSION_MAJOR,
  ["version-minor"]   = c_VERSION_MINOR,
  ["version-patch"]   = c_VERSION_PATCH,
  
  -- General settings
  ["stats-mode"]      = c_MODE_LOCAL,    -- Retrieve stats local (c_MODE_LOCAL) or via getstats (c_MODE_GETSTATS)
  ["stats-rangedps"]  = true,            -- Calculate Range AA dps (true) or per hit (false) 
  ["stats-meleedps"]  = true,            -- Calculate Melee AA dps (true) or per hit (false) 
  
  -- General UI settings
  ["ui-mode"]         = c_UI_FULL,       -- Use Full UI (c_UI_FULL) or light (c_UI_LIGHT)
  ["ui-hidden"]       = false,           -- Show statdoll (true) or not (false)
  ["ui-locked"]       = false,           -- Statdoll movable (false) or not (true)
  ["ui-background"]   = true,            -- Show Background (true) or not (false) 
  ["ui-background-alpha"] = 0.5,         -- 
  ["ui-close-button"] = false,           -- Toggle visibility of close button
  ["ui-scale-factor"] = 1,               -- 
  
  -- Full UI settings
  ["ui-pos-x"] = 100,    
  ["ui-pos-y"] = 100,
  ["ui-show-magic"] = true,              -- Toggle Visibility of subwindow heal/magic-power
  ["ui-show-range"] = true,              -- Toggle Visibility of subwindow rangepower
  ["ui-show-melee"] = true,              -- Toggle Visibility of subwindow meleepower

  -- Light UI Settings
  ["ui-light-pos-x"] = 100,
  ["ui-light-pos-y"] = 100,
  ["ui-light-show-healpower"]  = false,  -- Show Healpower + Healcrit in Light UI
  ["ui-light-show-magicpower"] = false,  -- Show Magicpower + Magiccrit in Light UI
  ["ui-light-show-rangepower"] = false,  -- Show Rangepower + Rangecrit in Light UI
  ["ui-light-show-meleepower"] = false,  -- Show Meleepower + Meleecrit in Light UI
  ["ui-light-additional"] = true,       -- Show additional stats in Light UI
}

------------------------------
-- Locals
--

-- Statdoll.onInitialize
function Statdoll.onInitialize()
  -- initialize settings
  if (Statdoll.settings == nil) then
    Statdoll.settings = Statdoll.defaultSettings
  end
  
  -- Register Event Listeners 
  RegisterEventHandler( SystemData.Events.LOADING_END,  "Statdoll.onLoad")
  RegisterEventHandler( SystemData.Events.RELOAD_INTERFACE,  "Statdoll.onLoad")
  
  -- creating the window
  Statdoll.createStatdollWindow()
  
  -- initialize options
  Statdoll.Options.onInitialize()
  
  -- initialize stats and stat listeners
  Statdoll.Stats.onInitialize()
  
  TextLogAddEntry("Chat", 0, L"<icon=57> Statdoll Remix " .. c_VERSION_MAJOR .. L"." .. c_VERSION_MINOR .. L"." .. c_VERSION_PATCH .. L" Loaded")
end

-- Statdoll.onLoad
function Statdoll.onLoad()
  if(Statdoll.fullyLoaded == true) then return end
  
  -- Register slash commands, if libSlash is available
  if( LibSlash ~= nil ) then
    LibSlash.RegisterWSlashCmd("statdoll", Statdoll.onSlashCmd)
  end
  
  if (Statdoll.settings["ui-hidden"] == false) then
    Statdoll.show()
  end
  
  Statdoll.fullyLoaded = true
end

-- Statdoll.onShutdown
function Statdoll.onShutdown()
  Statdoll.statdollWnd.onShutdown()
end

-- Statdoll.createStatdollWindow
function Statdoll.createStatdollWindow()
  -- if a previous window is active, destroy it in a clean way 
  if(Statdoll.statdollWnd ~= nil) then
    Statdoll.statdollWnd.onShutdown()
  end
  
  Statdoll.statdollWnd = Statdoll.settings["ui-mode"] == c_UI_FULL
    and StatdollWnd
    or  StatdollWndLight
  
  Statdoll.statdollWnd.onInitialize()
  Statdoll.statdollWnd.setLocked(Statdoll.settings["ui-locked"]) 
  Statdoll.statdollWnd.setBackground(Statdoll.settings["ui-background"])
  Statdoll.statdollWnd.setBackgroundAlpha(Statdoll.settings["ui-background-alpha"])
  Statdoll.statdollWnd.setScaleFactor(Statdoll.settings["ui-scale-factor"])
  Statdoll.statdollWnd.setCloseButtonVisible(Statdoll.settings["ui-close-button"])
  
  if (Statdoll.settings["ui-hidden"] == false and Statdoll.fullyLoaded == true) then
    Statdoll.show()
  end
end

-- Statdoll.onRButtonUP
function Statdoll.onRButtonUp() 
  Statdoll.Stats.listener.setReferenceValues()
  Statdoll.Stats.onStatsUpdated()
end

-- Statdoll.onMButtonUp
function Statdoll.onMButtonUp()
  Statdoll.Options.show()
end

-- Statdoll.onSlashCmd
function Statdoll.onSlashCmd(args)
  -- show options window
  if (args == nil or args == L"") then
    Statdoll.Options.show()
    return
  end
  
  -- show statdoll
  if (args == L"show") then
    Statdoll.show()
    return
  end
  
  -- hide statdoll
  if (args == L"hide") then
    Statdoll.hide()
    return
  end
  
  -- TODO: show help message
end

-- Statdoll.show
function Statdoll.show() 
  Statdoll.settings["ui-hidden"] = false
  Statdoll.statdollWnd.setVisible(true)
  
  --  notify listener that window is shown
  Statdoll.Stats.listener.onWindowShown()
end

-- Statdoll.hide
function Statdoll.hide() 
  Statdoll.settings["ui-hidden"] = true
  Statdoll.statdollWnd.setVisible(false)

  -- notify listener that window is hidden
  Statdoll.Stats.listener.onWindowHidden()
end

-- Statdoll.toggleMeleeDPS
function Statdoll.toggleMeleeDPS()
  Statdoll.Stats.setMeleeDPS()
  Statdoll.statdollWnd.setLabels()
  Statdoll.Stats.listener.forceUpdate()
end

-- Statdoll.toggleRangeDPS
function Statdoll.toggleRangeDPS()
  Statdoll.Stats.setRangeDPS()
  Statdoll.statdollWnd.setLabels()
  Statdoll.Stats.listener.forceUpdate()
end
