<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="Statdoll Remix" version="0.9.0" date="29/04/2017">
        <Author name="Sullemunk, Bklopptah" />
        <Description text="Statdoll Remix, Provides character attributes in a more compact frame. This Remix combines functionality of Statdoll (Full/Light) and Getstats." />
		<Dependencies>
			<Dependency name="LibSlash" />
		</Dependencies>
        <Files>
            <File name="Statdoll.lua" />
            <File name="StatdollStats.lua" />
            <File name="StatdollValues.lua" />
            <File name="StatdollLocal.lua" />
            <File name="StatdollGetstats.lua" />
            <File name="StatdollOptions.lua" />
            <File name="StatdollOptions.xml" />
            <File name="StatdollWnd.lua" />
            <File name="StatdollWnd.xml" />
            <File name="StatdollWndLight.lua" />
            <File name="StatdollWndLight.xml" />
		</Files>
		<SavedVariables>         
      		<SavedVariable name="Statdoll.settings"/>      
    	</SavedVariables>     
    	         
        <OnInitialize>
            <CallFunction name="Statdoll.onInitialize" />
        </OnInitialize>
        <OnUpdate />
        <OnShutdown>
        	<CallFunction name="Statdoll.onShutdown" />
        </OnShutdown>
    </UiMod>
</ModuleFile>