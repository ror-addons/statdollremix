if (Statdoll == nil) then
  Statdoll = {}
end

if (Statdoll.Options == nil) then
  Statdoll.Options = {}
end

Statdoll.Options.window = "StatdollOptions"

function Statdoll.Options.onInitialize()
  -- creating the window
  CreateWindow(Statdoll.Options.window, false)
  
  LabelSetText(Statdoll.Options.window .. "Title", L"Statdoll Remix Options")
  
  local strVersion = "v" .. Statdoll.settings["version-major"] .. "." .. Statdoll.settings["version-minor"] .. "." .. Statdoll.settings["version-patch"]
  LabelSetText(Statdoll.Options.window .. "ButtonGroup" .. "Version", towstring(strVersion))
  
  ButtonSetText(Statdoll.Options.window .. "ButtonGroup" .. "Cancel", L"Close")
  ButtonSetText(Statdoll.Options.window .. "ButtonGroup" .. "Apply", L"Apply")
  
  -- options
  LabelSetText(Statdoll.Options.window .. "Enable" .. "Label", L"Show Statdoll")
  
  LabelSetText(Statdoll.Options.window .. "GUI" .. "Label", L"UI Version:")
  ComboBoxClearMenuItems(Statdoll.Options.window .. "GUI" .. "Combobox")
  ComboBoxAddMenuItem(Statdoll.Options.window .. "GUI" .. "Combobox", L"Full")
  ComboBoxAddMenuItem(Statdoll.Options.window .. "GUI" .. "Combobox", L"Light")
  ComboBoxSetSelectedMenuItem(Statdoll.Options.window .. "GUI" .. "Combobox", 1)
  
  LabelSetText(Statdoll.Options.window .. "Mode" .. "Label", L"Stat Mode:")
  ComboBoxClearMenuItems(Statdoll.Options.window .. "Mode" .. "Combobox")
  ComboBoxAddMenuItem(Statdoll.Options.window .. "Mode" .. "Combobox", L"Local")
  ComboBoxAddMenuItem(Statdoll.Options.window .. "Mode" .. "Combobox", L"Getstats")
  ComboBoxSetSelectedMenuItem(Statdoll.Options.window .. "Mode" .. "Combobox", 1)
  
  LabelSetText(Statdoll.Options.window .. "Lock" .. "Label", L"Lock Window")
  LabelSetText(Statdoll.Options.window .. "CloseButton" .. "Label", L"Show Close Button")
  LabelSetText(Statdoll.Options.window .. "ShowBackground" .. "Label", L"Show Background")
  
  LabelSetText(Statdoll.Options.window .. "BackgroundAlpha" .. "Label", L"Background alpha: ")
  LabelSetText(Statdoll.Options.window .. "BackgroundAlpha" .. "Value", L"0")
  
  LabelSetText(Statdoll.Options.window .. "ScaleFactor" .. "Label", L"Scale Factor: ")
  LabelSetText(Statdoll.Options.window .. "ScaleFactor" .. "Value", L"0")
end

function Statdoll.Options.show()
  if (WindowGetShowing(Statdoll.Options.window) == false) then
    Statdoll.Options.settingsToGUI()
    WindowSetShowing(Statdoll.Options.window, true)
  end
end

function Statdoll.Options.hide()
  if (WindowGetShowing(Statdoll.Options.window) == true) then
    WindowSetShowing(Statdoll.Options.window, false)
  end
end

function Statdoll.Options.onAlphaSlide(value)
  local alpha = tonumber(wstring.format(L"%.02f", value))
  LabelSetText(Statdoll.Options.window .. "BackgroundAlpha" .. "Value", towstring(alpha))
  
  Statdoll.statdollWnd.setBackgroundAlpha(alpha)
end

function Statdoll.Options.onScaleSlide(value)
  local scale = tonumber(wstring.format(L"%.02f", value + 0.5))
  LabelSetText(Statdoll.Options.window .. "ScaleFactor" .. "Value", towstring(scale))
  
  Statdoll.statdollWnd.setScaleFactor(scale)
end

function Statdoll.Options.onApply()
  local btnValue = nil
  
  -- show/hide statdoll
  btnValue = ButtonGetPressedFlag(Statdoll.Options.window .. "Enable" .. "Button")
  if(btnValue == true and WindowGetShowing(Statdoll.statdollWnd.window) == false) then
    Statdoll.show()
  elseif(btnValue == false and WindowGetShowing(Statdoll.statdollWnd.window) == true) then
    Statdoll.hide()
  end
  
  -- lock window
  btnValue = ButtonGetPressedFlag(Statdoll.Options.window .. "Lock" .. "Button")
  Statdoll.settings["ui-locked"] = btnValue
  Statdoll.statdollWnd.setLocked(btnValue)
  
  -- show close button
  btnValue = ButtonGetPressedFlag(Statdoll.Options.window .. "CloseButton" .. "Button")
  Statdoll.settings["ui-close-button"] = btnValue
  Statdoll.statdollWnd.setCloseButtonVisible(btnValue)
  
  -- show background
  btnValue = ButtonGetPressedFlag(Statdoll.Options.window .. "ShowBackground" .. "Button")
  Statdoll.settings["ui-background"] = btnValue
  Statdoll.statdollWnd.setBackground(btnValue)
  
  -- background alpha
  btnValue = SliderBarGetCurrentPosition(Statdoll.Options.window .. "BackgroundAlpha" .. "Slider")
  local alpha = tonumber(wstring.format(L"%.02f", btnValue))
  Statdoll.settings["ui-background-alpha"] = alpha
  Statdoll.statdollWnd.setBackgroundAlpha(alpha)

  -- scale factor
  btnValue = SliderBarGetCurrentPosition(Statdoll.Options.window .. "ScaleFactor" .. "Slider")
  local scale = tonumber(wstring.format(L"%.02f", btnValue + 0.5))
  Statdoll.settings["ui-scale-factor"] = scale
  Statdoll.statdollWnd.setScaleFactor(scale)
    
  -- switch ui version
  btnValue = ComboBoxGetSelectedMenuItem(Statdoll.Options.window .. "GUI" .. "Combobox")
  if(btnValue ~= Statdoll.settings["ui-mode"]) then
    Statdoll.settings["ui-mode"] = btnValue
    Statdoll.createStatdollWindow()
    Statdoll.statdollWnd.onUpdate()
  end
  
  -- switch retrieval mode
  btnValue = ComboBoxGetSelectedMenuItem(Statdoll.Options.window .. "Mode" .. "Combobox")
  if(btnValue ~= Statdoll.settings["stats-mode"]) then
    Statdoll.settings["stats-mode"] = btnValue
    Statdoll.Stats.onShutdown()
    Statdoll.Stats.onInitialize()
    if(WindowGetShowing(Statdoll.statdollWnd.window) == true) then
      Statdoll.Stats.listener.onWindowShown()
    end
  end
  
  -- close the options window
  --Statdoll.Options.hide()
end

function Statdoll.Options.onCancel()
  Statdoll.statdollWnd.setBackgroundAlpha(Statdoll.settings["ui-background-alpha"])
  Statdoll.statdollWnd.setScaleFactor(Statdoll.settings["ui-scale-factor"])
  Statdoll.Options.hide()
end

function Statdoll.Options.settingsToGUI()
  ButtonSetPressedFlag(Statdoll.Options.window .. "Enable" .. "Button" , not Statdoll.settings["ui-hidden"])
  ComboBoxSetSelectedMenuItem(Statdoll.Options.window .. "GUI" .. "Combobox", Statdoll.settings["ui-mode"])
  ComboBoxSetSelectedMenuItem(Statdoll.Options.window .. "Mode" .. "Combobox", Statdoll.settings["stats-mode"])
  ButtonSetPressedFlag(Statdoll.Options.window .. "Lock" .. "Button" , Statdoll.settings["ui-locked"])
  ButtonSetPressedFlag(Statdoll.Options.window .. "CloseButton" .. "Button" , Statdoll.settings["ui-close-button"])
  ButtonSetPressedFlag(Statdoll.Options.window .. "ShowBackground" .. "Button" , Statdoll.settings["ui-background"])
  
  local alpha = tonumber(wstring.format(L"%.02f", Statdoll.settings["ui-background-alpha"]))
  LabelSetText(Statdoll.Options.window .. "BackgroundAlpha" .. "Value", towstring(alpha))
  SliderBarSetCurrentPosition(Statdoll.Options.window .. "BackgroundAlpha" .. "Slider" , alpha)
  
  local scale = tonumber(wstring.format(L"%.02f", Statdoll.settings["ui-scale-factor"]))
  LabelSetText(Statdoll.Options.window .. "ScaleFactor" .. "Value", towstring(scale))
  SliderBarSetCurrentPosition(Statdoll.Options.window .. "ScaleFactor" .. "Slider" , scale - 0.5)
end
