if (Statdoll == nil) then
  Statdoll = {}
end

------------------------------
-- Statdoll.Getstats
--
Statdoll.Getstats = {
}

------------------------------
-- Locals
--
local prevStatsSet = false
local populating   = false
local betterStatNames = {
  ["ActionPointRegen"] = "AP Regen",
  ["MoraleRegen"]      = "Morale Regen",
  ["CriticalHitRateReduction"] = "Red. Critchance",
  ["OutgoingDamagePercent"] = "Out Dmg Mod",
  ["ArmorPenetrationReduction"] = "Red. Armorpen.",
  ["HealthRegen"] = "HP Regen",
  ["DisruptStrikethrough"] = "Distrupt Strike",
  ["Mastery1Bonus"] = "Mastery 1",
  ["Mastery2Bonus"] = "Mastery 2",
  ["Mastery3Bonus"] = "Mastery 3",
}

local betterStatNamesShort = {
  ["ActionPointRegen"] = "+AP:",
  ["MoraleRegen"]      = "+Mor:",
  ["OutgoingDamagePercent"] = "xDmg:",
  ["CriticalHitRateReduction"] = "-Crit:",
  ["ArmorPenetrationReduction"] = "-Pen:",
  ["HealthRegen"] = "+HP/s",
  ["DisruptStrikethrough"] = "-Dis",
  ["Mastery1Bonus"] = "Mas 1",
  ["Mastery2Bonus"] = "Mas 2",
  ["Mastery3Bonus"] = "Mas 3",
}

-- Statdoll.Getstats.onInitialize
function Statdoll.Getstats.onInitialize()
  prevStatsSet = false
  Statdoll.Getstats.registerEventHandler()
end

-- Statdoll.Getstats.onShutdown
function Statdoll.Getstats.onShutdown()
  Statdoll.Getstats.unregisterEventHandler()
end

-- Statdoll.Getstats.onUpdate
function Statdoll.Getstats.onUpdate(updateType, filterType)
  if(updateType ~= SystemData.TextLogUpdate.ADDED ) then return end

  -- get last 'system' message
  local _, _, line = TextLogGetEntry( "System", TextLogGetNumEntries("System") - 1 )

  -- check for the first getstats line, if found ignore it and start with next line
  if(populating == false and line:find(L"Stats for")) then
    populating = true

    -- TODO: implement, manual setting of stats while using getstats
    Statdoll.Getstats.setReferenceValues()

    -- set deprecated flag, to detect removed stats
    for i, statArray in pairs(Statdoll.Stats.additionalValues) do
      statArray["value"][1]   = 0
      statArray["value"][2]   = 0
      statArray["deprecated"] = true
    end
    return
  end

  -- ignore any non getstats messages
  if(populating == false) then return end

  -- check for the last getstats line
  if(populating == true and line:find(L"Item Stat Total")) then
    populating = false

    -- if this is the first set of values gathered, set this set as reference -> no green coloring
    if(prevStatsSet == false) then
      Statdoll.Getstats.setReferenceValues()
      prevStatsSet = true
    end
    
    -- remove any deprecated additional values
    for i, statArray in pairs(Statdoll.Stats.additionalValues) do
      if(statArray["deprecated"] == true and (statArray["refValue"][1] + statArray["refValue"][2]) == 0) then
        table.remove(Statdoll.Stats.additionalValues, i)
      end 
    end

    -- notify Statdoll that stats are updated and show the window
    Statdoll.Stats.onStatsUpdated()
    Statdoll.show()

    return
  end

  -- extract stat and value from current line
  local stat, value, _  = string.match(tostring(line), "([A-Za-z0-9]+)%s([0-9]+)%s(.*)")
  
  if(stat == nil or value == nil) then
    stat, value = string.match(tostring(line), "([A-Za-z0-9]+)%sx([0-9.]+)")
    if(stat == nil or value == nil) then
      return
    end
  end
  
  -- try to find stat in basic stats table
  local index = Statdoll.Getstats.findIndex(Statdoll.Stats.values, stat)
  if(index ~= nil) then
    Statdoll.Stats.values[index]["value"][1] = tonumber(value)
    Statdoll.Getstats.setPowerValue(index, Statdoll.Stats.values[index])
    return
  end

  -- try to find stat in additional stats table
  local index = Statdoll.Getstats.findIndex(Statdoll.Stats.additionalValues, stat)
  if(index ~= nil) then
    Statdoll.Stats.additionalValues[index]["value"][1]   = tonumber(value)
    Statdoll.Stats.additionalValues[index]["deprecated"] = false
    return
  end

  -- stat was not previously added
  local statArray = {
    ["refWnd"]       = stat,
    ["lookupStr"]    = stat,

    ["txtLabel"]     = Statdoll.Getstats.getBetterName(stat),
    ["txtLabelS"]    = Statdoll.Getstats.getBetterNameShort(stat),
    ["txtIcon"]      = L"<icon=100>",
    ["txtPowerIcon"] = L"<icon=156>",

    ["color-r"]      = 255,
    ["color-g"]      = 255,
    ["color-b"]      = 255,

    ["refValue"]      = {0, 0},
    ["value"]         = {tonumber(value), 0},
    ["txtValue"]      = L"",
    ["txtPowerValue"] = L"",

    format = nil
  }
  
  -- set reference only if there is no previous set of references
  if(prevStatsSet == false) then
    statArray["refValue"][1] = tonumber(value)
  end
  
  table.insert(Statdoll.Stats.additionalValues, statArray)
end

-- Statdoll.Getstats.registerEventHandler
function Statdoll.Getstats.registerEventHandler()
  RegisterEventHandler(TextLogGetUpdateEventId("System"), "Statdoll.Getstats.onUpdate")
end

-- Statdoll.Getstats.unregisterEventHandler
function Statdoll.Getstats.unregisterEventHandler()
  UnregisterEventHandler(TextLogGetUpdateEventId("System"), "Statdoll.Getstats.onUpdate")
end

-- Statdoll.Getstats.onWindowShown
function Statdoll.Getstats.onWindowShown()
end

-- Statdoll.Getstats.onWindowHidden
function Statdoll.Getstats.onWindowHidden()
end

-- Statdoll.Getstats.forceUpdate
function Statdoll.Getstats.forceUpdate()
  Statdoll.Stats.onStatsUpdated()
  Statdoll.show()
end

-- Statdoll.Getstats.setReferenceValues
function Statdoll.Getstats.setReferenceValues()
  Statdoll.Stats.refValuesSet = true

  -- set reference for built-in stats
  for i, statArray in pairs(Statdoll.Stats.values) do
    statArray["refValue"][1] = statArray["value"][1]
    statArray["refValue"][2] = statArray["value"][2]
  end

  -- set reference for additional stats
  for i, statArray in pairs(Statdoll.Stats.additionalValues) do
    statArray["refValue"][1] = statArray["value"][1]
    statArray["refValue"][2] = statArray["value"][2]
  end
end

-- Statdoll.Getstats.lookupByStr
function Statdoll.Getstats.findIndex(tValue, lookupStr)
  -- Try to find the array of 'str' and return the index
  for i, statArray in pairs(tValue) do
    if(statArray["lookupStr"] == lookupStr) then
      return i
    end
  end
  
  -- otherwise nil
  return nil
end


-- Statdoll.Getstats.setPowerValue
function Statdoll.Getstats.setPowerValue(index, baseStatArray)
  if(index == Statdoll._STRENGTH) then
    Statdoll.Stats.values[Statdoll._MELEEPOWER]["value"][2]   = baseStatArray["value"][1]
    --Statdoll.Stats.values[Statdoll._MELEEAAPOWER]["value"][1] = baseStatArray["value"][1]
    return
  end
  
  if(index == Statdoll._BALLISTIC) then
    Statdoll.Stats.values[Statdoll._RANGEPOWER]["value"][2]   = baseStatArray["value"][1]
    --Statdoll.Stats.values[Statdoll._RANGEAAPOWER]["value"][1] = baseStatArray["value"][1]
    return
  end
  
  if(index == Statdoll._WILLPOWER) then
    Statdoll.Stats.values[Statdoll._HEALPOWER]["value"][2] = baseStatArray["value"][1]
    return
  end
  
  if(index == Statdoll._INTELLIGENCE) then
    Statdoll.Stats.values[Statdoll._MAGICPOWER]["value"][2] = baseStatArray["value"][1]
    return
  end
end

-- Statdoll.Getstats.getBetterName
function Statdoll.Getstats.getBetterName(fullName)
  return betterStatNames[fullName] or fullName
end

-- Statdoll.Getstats.getBetterName
function Statdoll.Getstats.getBetterNameShort(fullName)
  return betterStatNamesShort[fullName] or fullName
end